FROM mcr.microsoft.com/dotnet/sdk:3.1
ARG EXECUTABLE

COPY . /app
WORKDIR /app
#COPY /publish .

ENV ASPNETCORE_URLS "http://*:5000"
ENV ASPNETCORE_ENVIRONMENT "Staging"
RUN dotnet restore "/app/FormConstructorService.csproj"
RUN dotnet test
RUN dotnet publish -c Release -o /publish

CMD ["dotnet", "$EXECUTABLE"]
