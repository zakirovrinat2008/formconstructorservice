using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormConstructorService.Exceptions;
using FormConstructorService.Models;
using FormConstructorService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FormConstructorService.Controllers
{
    [ApiController]
    [Route("api/FormConfigController")]
    public class FormConfigController : ControllerBase
    {
        private readonly IFormConfigService _formConfigService;
        
        public FormConfigController(IFormConfigService formConfigService)
        {
            _formConfigService = formConfigService;
        }
        
        [HttpPost("SaveFormConfig")]
        public async Task<FormConfigModel> SaveForm(FormConfigModel formConfig)
        {
            return await _formConfigService.SaveFormConfig(formConfig);
        }
        
        [HttpGet("GetAllFormConfigs")]
        public async Task<List<FormConfigShortModel>> GetAllFormConfigs()
        {
            return await _formConfigService.GetAllFormConfigs();
        }
        
        [HttpGet("GetFormConfigById")]
        public async Task<ActionResult<FormConfigModel>> GetFormConfigById(Guid id)
        {
            try
            {
                return await _formConfigService.GetFormConfigById(id);
            }
            catch (FormNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }
    }
}