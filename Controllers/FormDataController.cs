using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormConstructorService.Exceptions;
using FormConstructorService.Models;
using FormConstructorService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FormConstructorService.Controllers
{
    [ApiController]
    [Route("api/FormDataController")]
    public class FormDataController : ControllerBase
    {
        private readonly IFormDataService _formDataService;
        
        public FormDataController(IFormDataService formDataService)
        {
            _formDataService = formDataService;
        }
        
        [HttpPost("SaveFormData")]
        public async Task<FormDataModel> SaveFormData(FormDataModel formData)
        {
            return await _formDataService.SaveFormData(formData);
        }
        
        [HttpGet("GetFormDataById")]
        public async Task<FormDataModel> GetFormDataById(Guid id)
        {
            return await _formDataService.GetFormDataById(id);
        }

        [HttpPost("GetAllFormDataByFormId")]
        public async Task<List<FormDataModel>> GetAllFormDataByFormId(Guid formId)
        {
            return await _formDataService.GetAllFormDataByFormId(formId);
        }

        [HttpGet("GetAllShortFormDataByFormId")]
        public async Task<List<FormDataShortModel>> GetAllShortFormDataByFormId(Guid formId)
        {
            return await _formDataService.GetAllShortFormDataByFormId(formId);
        }
    }
}