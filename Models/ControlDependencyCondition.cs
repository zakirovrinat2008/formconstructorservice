namespace FormConstructorService.Models
{
    public class ControlDependencyCondition
    {
        public string ControlName { get; set; }
        public bool HasValue { get; set; }
    }
}