using System.Collections.Generic;
using FormConstructorService.Enums;

namespace FormConstructorService.Models
{
    public class ControlConfig
    {
        public string ControlName { get; set; }
        public ControlTypesEnum Type { get; set; }
        public string Label { get; set; }
        public List<ControlValidatorsEnum> Validators { get; set; }
        public ControlDependencyCondition DisabledCondition { get; set; }
        public ControlDependencyCondition RequiredCondition { get; set; }
        public List<string> GroupControls { get; set; }
    }
}