using System;

namespace FormConstructorService.Models
{
    public class FormDataShortModel
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public string UserName { get; set; }
    }
}