using System;
using System.Collections.Generic;

namespace FormConstructorService.Models
{
    public class FormDataModel
    {
        public Guid? Id { get; set; }
        public Guid FormId { get; set; }
        public string UserName { get; set; }
        public List<ControlsValues> Data { get; set; }
    }
}