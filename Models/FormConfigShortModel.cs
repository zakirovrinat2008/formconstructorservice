using System;
using System.Collections.Generic;

namespace FormConstructorService.Models
{
    public class FormConfigShortModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}