using System;
using System.Collections.Generic;

namespace FormConstructorService.Models
{
    public class FormConfigModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public List<ControlConfig> FormControls { get; set; }
    }
}