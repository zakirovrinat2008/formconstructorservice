using System;

namespace FormConstructorService.Models
{
    public class ControlsValues
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
    }
}