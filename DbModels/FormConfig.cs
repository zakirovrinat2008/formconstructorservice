using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FormConstructorService.DbModels
{
    [Table("FormConfig")]
    public class FormConfig
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FormControls { get; set; }
    }
}