using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FormConstructorService.DbModels
{
    [Table("FormData")]
    public class FormData
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public string UserName { get; set; }
        public string Data { get; set; }
    }
}