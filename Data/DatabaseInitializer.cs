using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace FormConstructorService.Data
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly DataContext _dataContext;

        public DatabaseInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task SeedAsync()
        {

            await _dataContext.Database.MigrateAsync();
        }
    }
}