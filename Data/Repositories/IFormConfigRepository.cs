using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormConstructorService.DbModels;
using FormConstructorService.Models;

namespace FormConstructorService.Data.Repositories
{
    public interface IFormConfigRepository
    {
        Task<FormConfig> AddFormConfig(FormConfig formConfig);
        
        Task<List<FormConfig>> GetAllFormConfigs();

        Task<FormConfig> GetFormConfigById(Guid id);
    }
}