using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormConstructorService.DbModels;
using FormConstructorService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace FormConstructorService.Data.Repositories
{
    public class FormConfigRepository : IFormConfigRepository
    {
        private readonly DbSet<FormConfig> _dbSet;
        private readonly DataContext _dataContext;

        public FormConfigRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<FormConfig>();
        }

        public async Task<FormConfig> AddFormConfig(FormConfig formConfig)
        {
            await _dbSet.AddAsync(formConfig);
            await _dataContext.SaveChangesAsync();
            return await _dbSet.FirstOrDefaultAsync(x => x.Id == formConfig.Id);
        }

        public async Task<List<FormConfig>> GetAllFormConfigs()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<FormConfig> GetFormConfigById(Guid id)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}