using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormConstructorService.DbModels;
using FormConstructorService.Models;

namespace FormConstructorService.Data.Repositories
{
    public interface IFormDataRepository
    {
        Task<FormData> AddFormData(FormData formData);
        
        Task<List<FormData>> GetAllFormData();

        Task<FormData> GetFormDataById(Guid id);
        
        Task<List<FormData>> GetAllFormDataByFormId(Guid formId);
    }
}