using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormConstructorService.DbModels;
using FormConstructorService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace FormConstructorService.Data.Repositories
{
    public class FormDataRepository : IFormDataRepository
    {
        private readonly DbSet<FormData> _dbSet;
        private readonly DataContext _dataContext;

        public FormDataRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<FormData>();
        }
        
        public async Task<FormData> AddFormData(FormData formData)
        {
            await _dbSet.AddAsync(formData);
            await _dataContext.SaveChangesAsync();
            return await _dbSet.FirstOrDefaultAsync(x => x.Id == formData.Id);
        }

        public async Task<List<FormData>> GetAllFormData()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<FormData> GetFormDataById(Guid id)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<FormData>> GetAllFormDataByFormId(Guid formId)
        {
            return await _dbSet.Where(x=>x.FormId == formId).ToListAsync();
        }
    }
}