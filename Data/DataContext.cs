using FormConstructorService.DbModels;
using Microsoft.EntityFrameworkCore;

namespace FormConstructorService.Data
{
    public class DataContext : DbContext
    {
        public DbSet<FormConfig> FormConfigs { get; set; }
        private DbSet<FormData> FormData { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }
    }
}