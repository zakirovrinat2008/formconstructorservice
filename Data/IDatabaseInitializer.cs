using System.Threading.Tasks;

namespace FormConstructorService.Data
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }
}