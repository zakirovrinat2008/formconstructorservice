namespace FormConstructorService.Enums
{
    public enum ControlTypesEnum
    {
        String,
        Number,
        Date,
        Checkbox
    }
}