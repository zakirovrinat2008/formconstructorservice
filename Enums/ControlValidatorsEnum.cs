namespace FormConstructorService.Enums
{
    public enum ControlValidatorsEnum
    {
        Required,
        MaxLength,
        MinLength,
    }
}