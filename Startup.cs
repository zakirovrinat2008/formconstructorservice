using System.Text.Json.Serialization;
using FormConstructorService.Data;
using FormConstructorService.Data.Repositories;
using FormConstructorService.Models;
using FormConstructorService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

namespace FormConstructorService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private IConfiguration _configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddTransient<IFormConfigRepository, FormConfigRepository>();
            services.AddTransient<IFormConfigService, FormConfigService>();
            services.AddTransient<IFormDataService, FormDataService>();
            services.AddTransient<IFormDataRepository, FormDataRepository>();
            services.AddTransient<IDatabaseInitializer, DatabaseInitializer>();
            
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "FormConstructorService", Version = "v1"});
            });
            
            services.Configure<DbSettings>(_configuration.GetSection("DbSettings"));

            services.AddDbContext<DataContext>(options =>
            {
                options.UseNpgsql(_configuration["DbSettings:ConnectionString"],
                    o => o.MigrationsAssembly("FormConstructorService"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseCors(builder =>
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FormConstructorService"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}