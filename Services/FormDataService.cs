using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormConstructorService.Data.Repositories;
using FormConstructorService.DbModels;
using FormConstructorService.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FormConstructorService.Services
{
    public class FormDataService : IFormDataService
    {
        private ILogger<FormConfigService> _logger;
        private IFormDataRepository _formDataRepository;

        public FormDataService(ILogger<FormConfigService> logger, IFormDataRepository formDataRepository)
        {
            _logger = logger;
            _formDataRepository = formDataRepository;
        }

        public async Task<FormDataModel> SaveFormData(FormDataModel formDataModel)
        {
            _logger.LogInformation("in SaveFormData\n" + JsonConvert.SerializeObject(formDataModel));
            var formData = await _formDataRepository.AddFormData(MapToFormData(formDataModel));
            var res = MapToFormDataModel(formData);
            _logger.LogInformation("out SaveFormData\n" + JsonConvert.SerializeObject(res));
            return res;
        }
        
        public async Task<FormDataModel> GetFormDataById(Guid id)
        {
            _logger.LogInformation("in GetFormDataById\n" + id);
            var formData = await _formDataRepository.GetFormDataById(id);
            var res = MapToFormDataModel(formData);
            _logger.LogInformation("out GetFormDataById\n" + JsonConvert.SerializeObject(res));
            return res;
        }
        
        public async Task<List<FormDataModel>> GetAllFormDataByFormId(Guid formId)
        {
            _logger.LogInformation("in GetAllFormDataByFormId\n" + formId);
            var formDataList = await _formDataRepository.GetAllFormDataByFormId(formId);
            var res = formDataList.Select(MapToFormDataModel).ToList();
            _logger.LogInformation("out GetAllFormDataByFormId\n" + JsonConvert.SerializeObject(res));
            return res;
        }
        
        public async Task<List<FormDataShortModel>> GetAllShortFormDataByFormId(Guid formId)
        {
            _logger.LogInformation("in GetAllShortFormDataByFormId\n" + formId);
            var formDataList = await _formDataRepository.GetAllFormDataByFormId(formId);
            var res = formDataList.Select(MapToFormDataShortModel).ToList();
            _logger.LogInformation("out GetAllShortFormDataByFormId\n" + JsonConvert.SerializeObject(res));
            return res;
        }

        private FormData MapToFormData(FormDataModel formDataModel)
        {
            return new FormData()
            {
                Id = formDataModel.Id??Guid.Empty,
                FormId = formDataModel.FormId,
                UserName = formDataModel.UserName,
                Data = JsonConvert.SerializeObject(formDataModel.Data)
            };
        }
        
        private FormDataModel MapToFormDataModel(FormData formData)
        {
            return new FormDataModel()
            {
                Id = formData.Id,
                FormId = formData.FormId,
                UserName = formData.UserName,
                Data = JsonConvert.DeserializeObject<List<ControlsValues>>(formData.Data)
            };
        }
        
        private FormDataShortModel MapToFormDataShortModel(FormData formData)
        {
            return new FormDataShortModel()
            {
                Id = formData.Id,
                FormId = formData.FormId,
                UserName = formData.UserName
            };
        }
    }
}