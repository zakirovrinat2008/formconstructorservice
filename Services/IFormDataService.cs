using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormConstructorService.Models;

namespace FormConstructorService.Services
{
    public interface IFormDataService
    {
        Task<FormDataModel> SaveFormData(FormDataModel formData);

        Task<FormDataModel> GetFormDataById(Guid id);
        
        Task<List<FormDataModel>> GetAllFormDataByFormId(Guid formId);
        
        Task<List<FormDataShortModel>> GetAllShortFormDataByFormId(Guid formId);
    }
}