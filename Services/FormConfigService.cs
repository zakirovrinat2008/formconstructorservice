using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormConstructorService.Data;
using FormConstructorService.Data.Repositories;
using FormConstructorService.DbModels;
using FormConstructorService.Exceptions;
using FormConstructorService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace FormConstructorService.Services
{
    public class FormConfigService : IFormConfigService
    {
        private readonly IFormConfigRepository _formConfigRepository;
        private ILogger<FormConfigService> _logger;

        public FormConfigService(IFormConfigRepository formConfigRepository, ILogger<FormConfigService> logger)
        {
            _formConfigRepository = formConfigRepository;
            _logger = logger;
        }

        public async Task<FormConfigModel> SaveFormConfig(FormConfigModel formConfig)
        {
            _logger.LogInformation("in SaveFormConfig\n" + JsonConvert.SerializeObject(formConfig));
            var res = MapToFormConfigModel(await _formConfigRepository.AddFormConfig(MapToFormConfig(formConfig)));
            _logger.LogInformation("out SaveFormConfig\n" + JsonConvert.SerializeObject(res));
            
            return res;
        }

        public async Task<List<FormConfigShortModel>> GetAllFormConfigs()
        {
            _logger.LogInformation("in GetAllFormConfigs");
            var formConfigs = await _formConfigRepository.GetAllFormConfigs();
            var res = formConfigs.Select(MapToFormConfigShortModel).ToList();
            _logger.LogInformation("out GetAllFormConfigs\n" + JsonConvert.SerializeObject(res));
            
            return res;
        }

        public async Task<FormConfigModel> GetFormConfigById(Guid id)
        {
            _logger.LogInformation("in GetFormConfigById\n" + id);
            var formConfig = await _formConfigRepository.GetFormConfigById(id);
            if (formConfig == null)
            {
                throw new FormNotFoundException("Form not found");
            }
            var res = MapToFormConfigModel(formConfig);
            _logger.LogInformation("out GetFormConfigById\n" + JsonConvert.SerializeObject(res));
            
            return res;
        }

        private FormConfig MapToFormConfig(FormConfigModel model)
        {
            return new FormConfig()
            {
                Name = model.Name,
                FormControls = JsonConvert.SerializeObject(model.FormControls)
            };
        }
        
        private FormConfigModel MapToFormConfigModel(FormConfig model)
        {
            return new FormConfigModel()
            {
                Id = model.Id,
                Name = model.Name,
                FormControls = JsonConvert.DeserializeObject<List<ControlConfig>>(model.FormControls)
            };
        }
        
        private FormConfigShortModel MapToFormConfigShortModel(FormConfig model)
        {
            return new FormConfigShortModel()
            {
                Id = model.Id,
                Name = model.Name
            };
        }
    }
}