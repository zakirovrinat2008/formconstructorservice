using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormConstructorService.Models;

namespace FormConstructorService.Services
{
    public interface IFormConfigService
    {
        Task<FormConfigModel> SaveFormConfig(FormConfigModel formConfig);

        Task<List<FormConfigShortModel>> GetAllFormConfigs();

        Task<FormConfigModel> GetFormConfigById(Guid id);
    }
}