using System;

namespace FormConstructorService.Exceptions
{
    [Serializable]
    public class FormNotFoundException : Exception
    {
        public FormNotFoundException() { }

        public FormNotFoundException(string message) : base(message) { }
    }
}